FROM node:lts

# Create app directory
WORKDIR /usr/src/app

# Install rust and cargo
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# Set path to include cargo
ENV PATH="/root/.cargo/bin:${PATH}"

# Install WASM-pack
RUN cargo install wasm-pack
